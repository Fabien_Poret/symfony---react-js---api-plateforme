import axios from "axios";
import customersAPI from "./customersAPI";
import jwtDecode from "jwt-decode";
import { LOGIN_API, USERS_API } from "../config";


/*
* Requete HTTP d'authentification et stockage du token dans axios
*/
function connexion(credentials){
    return axios     
    .post(LOGIN_API, credentials)
    .then(response => response.data.token)
    .then(token => {
        window.localStorage.setItem("authToken", token);
        axios.defaults.headers["Authorization"] = 'Bearer ' + token;
        customersAPI.findAll().then(console.log);
    });
}

//Déconnexion 
//Je vide le tocken 
//Je surpprime le type d'authorization
function logout(){
    window.localStorage.removeItem("authToken");
    delete axios.defaults.headers["Authorization"];
}
 
function setAxiosToken(token) {
    axios.defaults.headers["Authorization"] = 'Bearer ' + token;
}

/**
 * Mise en place lors du chargement de l'application
 */
function setup(){
    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            setAxiosToken(token);
        } 
    }
}

/**
 * Permet de savoir si on est authentifié ou pas 
 * Return booléans
 */
function isAuthenticated(){
    // Voir si on a un token
    const token = window.localStorage.getItem("authToken");
    // Si le token est encore actif
    if(token){
        const { exp: expiration } = jwtDecode(token);
        if(expiration *1000 > new Date().getTime())
        {
            return true;
        } 
        return false;
    }
    return false;
}

function post(user){
    axios.post(USERS_API, user)
}


export default {
    connexion,
    logout,
    setup,
    isAuthenticated,
    post
};