import React, {useContext} from 'react';
import {Redirect, Route} from 'react-router-dom';
import AuthContext from '../contexts/AuthContext';

// Rediriger vers login si l'utilisateur n'est pas connecté
const PrivateRoute = ({path, component}) =>{
    const {isAuthenticated }= useContext(AuthContext);
    return isAuthenticated ? (
        <Route path={path} component={component}/>
    ) : (
        <Redirect to="/login"/>
    );
}

export default PrivateRoute;