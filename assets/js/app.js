//Les imports important
import React, {useState} from 'react';
import ReactDOM from 'react-dom'
import NavBar from './components/Navbar';
import HomePage from './pages/HomePage';
import { HashRouter, Switch, Route, withRouter} from "react-router-dom";
import CustomersPage from './pages/CustomersPage';
import CustomerPage from './pages/CustomerPage';
import InvoicesPage from './pages/InvoicesPage';
import LoginPage from './pages/LoginPage';
import usersAPI from './services/usersAPI';
import AuthContext from './contexts/AuthContext';
import PrivateRoute from './components/PrivateRoute';
import InvoicePage from './pages/InvoicePage';
import RegisterPage from './pages/RegisterPage';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
require('../css/app.css');
usersAPI.setup();



console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

// Route réellement différente 

const App = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(
        usersAPI.isAuthenticated()
    );


    const NavBarWithRouter = withRouter(NavBar);

    return <>
        <AuthContext.Provider value={{
            isAuthenticated,
            setIsAuthenticated
        }}>
            <HashRouter>
                <NavBarWithRouter/>
                <main className="container pt-5">
                    <Switch>
                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />
                        <PrivateRoute path="/invoices/:id" component={InvoicePage}/>
                        <PrivateRoute path="/invoices" component={InvoicesPage}/>
                        <PrivateRoute path="/customers/:id" component={CustomerPage}/>
                        <PrivateRoute path="/customers"component={CustomersPage}/>
                        <Route path="/" component={HomePage}/>
                    </Switch>
                </main>
            </HashRouter>
            <ToastContainer position={toast.POSITION.BOTTOM_LEFT}/>
        </AuthContext.Provider>
        
    </>
};

const rootElement = document.querySelector('#app');
ReactDOM.render(<App />, rootElement);