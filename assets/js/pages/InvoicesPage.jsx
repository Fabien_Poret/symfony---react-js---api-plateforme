import React, { useEffect, useState } from 'react';
import InvoicesAPI from '../services/invoicesAPI';
import Pagination from '../components/Pagination';
import moment from "moment";
import {Link} from "react-router-dom";
import TableLoader from '../components/loaders/TableLoader';

const STATUS_CLASSES = {
    PAID: "success",
    SENT: "info",
    CANCELLED: "danger"
}
const STATUS_LABELS = {
    PAID: "Payée",
    SENT: "Envoyée",
    CANCELLED: "Annulée"
}


const InvoicesPage = () => {

    const[invoices, setInvoices] = useState([]);
    const[currentPage, setCurrentPage] = useState(1);
    const[search, setSearch] = useState('');
    const itemsPerPage = 10;
    const [loading, setLoading] = useState(true);

    // Permet de récupérer toutes les factures
    const fetchInvoices = async () => {
        try{
            const data = await InvoicesAPI.findAll()
            setInvoices(data)
            setLoading(false);
        }catch(error){
            console.log(error.response)
        }
    }
    // Au chargement du composant on va chercher les invoices
    useEffect(() => {
        fetchInvoices();
    }, []);

    const formatDate = (str) =>moment(str).format('DD/MM/YYYY');

    //Gestion de la suppression d'un customer
    const handleDelete = async id => {
        const originalInvoices = [...invoices];
        setInvoices(invoices.filter(invoices => invoices.id !== id));

        try{
            await InvoicesAPI.delete(id);
        }catch(error){
            setInvoices(originalInvoices);
        }
    };

    //Gestion du changement de page
    const handlePageChange = page => {
        setCurrentPage(page);
    }

    // Gestion de la recherche
    const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    }


    //Filtrage des customers en fonction de la recherche
    const filteredInvoices = invoices.filter(
        i => 
            i.customer.firstName.toLowerCase().includes(search.toLowerCase()) ||
            i.customer.lastName.toLowerCase().includes(search.toLowerCase()) ||
            // i.amount.toLowerCase().includes(search.toLowerCase()) ||
            STATUS_LABELS[i.status].toLowerCase().includes(search.toLowerCase()) 
    );

    // Pagination des données
    const paginatedInvoices = Pagination.getData(filteredInvoices, currentPage, itemsPerPage);
    

    return (
        <>
        <div className="table-responsive">
            
            <div className="d-flex justify-content-between align-items-center">
                <h1>Liste des factures</h1>
                <Link className="btn btn-info" to="/invoices/new">Créer une facture</Link>
            </div>

            <div className="form-group">
                <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher ..."/>
            </div> 

            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Numéro de facture</th>
                        <th>Client</th>
                        <th>Date</th>
                        <th className="text-center" >Statut</th>
                        <th className="text-center">Montant</th>
                        <th></th>
                    </tr>
                </thead>
                {!loading && (
                    <tbody>  
                    {paginatedInvoices.map(invoice => (
                        <tr key={invoice.id}>
                            <td><a href="#">{invoice.chrono}</a></td>
                            <td>
                                <Link to={"/customers/" + invoice.customer.id}>
                                    {invoice.customer.firstName} {invoice.customer.lastName}
                                </Link>
                            </td>
                            <td>{formatDate(invoice.sentAt)}</td>
                            <td>
                                <span className={"badge badge-" + STATUS_CLASSES[invoice.status]}>{STATUS_LABELS[invoice.status]}</span> 
                            </td>
                            <td className="text-center">{invoice.amount.toLocaleString()}</td>
                            <td>
                                <Link to={"/invoices/" + invoice.id} className="btn btn-sn btn-success mr-1">Editer</Link>
                                <button onClick={() => handleDelete(invoice.id)} className="btn btn-sn btn-danger">Supprimer</button>
                            </td>
                        </tr>
                    ))} 
                </tbody>
                )}
            </table>
            {loading && (<TableLoader/>)}

            {itemsPerPage < filteredInvoices.length && 
                <Pagination 
                    currentPage={currentPage} 
                    itemsPerPage={itemsPerPage} 
                    length={filteredInvoices.length} 
                    onPageChanged={handlePageChange}
                />
            }
        </div>
    </>

    );
}
 
export default InvoicesPage;