import React, {useState, useContext} from 'react';
import UsersAPI from '../services/usersAPI';
import AuthContext from '../contexts/AuthContext'
import Field from '../components/forms/Fields';
import { toast } from 'react-toastify';

const LoginPage = ({history }) => {

    const {setIsAuthenticated} = useContext(AuthContext);
    const[error, setError] = useState();

    const[credentials, setCredentials] = useState({
        username: "",
        password: ""
    });

    // Gestion des champs
    const handleChange = (event) => {
        const value = event.currentTarget.value;
        const name = event.currentTarget.name;
        setCredentials({...credentials, [name]: value});
    };
    
    // Gestion du submit
    const handleSubmit = async event => {
        event.preventDefault();
        try{
            await UsersAPI.connexion(credentials)
            setError("");
            setIsAuthenticated(true);
            toast.success("Vous êtes désormais connecté !");
            history.replace("/customers");
        }catch(error){
            setError("Aucun compte ne possède cette adresse email ou alors les informations ne correspondent pas");
            toast.error("Une erreur est survenue");
        }
    };

    return ( 
        <>
            <h1>Connexion à l'application</h1>
            <form onSubmit={handleSubmit}>
                <Field 
                label="Adresse email" 
                name="username" 
                value={credentials.username} 
                onChange={handleChange} 
                placeholder="Adresse email de connexion" 
                error={error}
                />
                <Field 
                label="Mot de passe" 
                name="password" 
                type="password"
                value={credentials.password} 
                onChange={handleChange} 
                placeholder="Mot de passe" 
                error={error}
                />
                <div className="form-group">
                    <button 
                        type="submit" 
                        className="btn btn-success">Connexion
                    </button>
                </div>
            </form>
        </>
    );
}
   
export default LoginPage;