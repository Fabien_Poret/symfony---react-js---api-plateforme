import React, {useState, useEffect } from 'react';
import Field from '../components/forms/Fields';
import FormContentLoader from '../components/loaders/FormContentLoader';
import { Link } from 'react-router-dom';
import customersAPI from '../services/customersAPI';
import { toast } from 'react-toastify';

const CustomerPage = ({ match, history}) => {

    const { id = "new"} = match.params;

    const[customer, setCustomer] = useState({
        lastName: "",
        firstName: "",
        email: "",
        company: ""
    });

    const [loading, setLoading] = useState(false);
    
    const[error, setError] = useState({
        lastName: "",
        firstName: "",
        email: "",
        company: ""
    });


    const [editing, setEditing] = useState(false);

    //Récupération d'un seul customer
    const fetchCustomer = async id => {
        try {
            //Je met les données du customers dans mon state pour pouvoir éditer
            const {firstName, lastName, email, company} = await customersAPI.find(id);
            setCustomer({firstName, lastName, email, company});
            setLoading(false);
            
        } catch (error) {
            console.log(error.response);
            toast.error("L'utilisateur n'a pas pu être récupéré")
        }
    }

    //Chargement du customer si besoin au chargement du composant ou au changement de l'identifiant
    useEffect(() => {
        if(id !== "new"){
            setLoading(true);
            setEditing(true);
            fetchCustomer(id);
        };
    }, [id])

    //Gestion des changements des inputs danns le formulaire
    const handleChange = ({ currentTarget}) => {
        const {name, value} = currentTarget;
        setCustomer({ ...customer, [name]: value });
    };

    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        //preventDefault permet de ne pas recharger la page
        event.preventDefault();
        try{

            if(editing){
                const response = await customersAPI.update(id, customer);
                setError({});
                toast.success("Le formulaire a bien été pris en compte");
            }
            else {
                await customersAPI.create(customer);
                setError({});
                toast.success("Le formulaire a bien été pris en compte");
                history.replace("/customers");
            }
        }catch({response}){
            const {violations} = response.data;

            if(violations){
                const apiErrors = {};
                violations.forEach(({ propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });

                setError(apiErrors);
                toast.error("Une erreur est survenue");
            }

        }
    };
    
    return (    
    <>
        {!editing &&  <h1>Création d'un customer</h1> || <h1>Modification du client</h1>}
        {loading && (<FormContentLoader/>)}
        
        {!loading && (
            <form onSubmit={handleSubmit}>
            <Field name="lastName" label="Nom de famille" error={error.lastName} placeholder="Nom de famille du client" value={customer.lastName} onChange={handleChange}/>
            <Field name="firstName" label="Prénom" error={error.firstName} placeholder="Prénom du client" value={customer.firstName} onChange={handleChange}/>
            <Field name="email" label="Email" error={error.email} placeholder="Email du client" value={customer.email} onChange={handleChange} />
            <Field name="company" label="Entreprise" error={error.company} placeholder="Entreprise du client" value={customer.company} onChange={handleChange} />
            <div className="form-group">
                <button type="submit" className="btn btn-success">
                    Enregistrer
                </button>
                <Link to="/customers" className="btn btn-link">Retour à la liste</Link>
            </div>
        </form>
        )}
    </>
     );
}
 
export default CustomerPage;